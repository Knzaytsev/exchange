<?php

namespace Interactors;

use Repositories\UserRepository;

class UserInteractor
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct($userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function registration($login, $password)
    {
        return $this->userRepository->registration($login, $password);
    }

    public function authorization($login, $password)
    {
        return $this->userRepository->authorization($login, $password);
    }

    public function getItems()
    {
        return $this->userRepository->getItems();
    }

    public function getStatusExchange()
    {
        return $this->userRepository->getStatusExchange();
    }

    public function getPeriodRevenue($from, $to)
    {
        return $this->userRepository->getRevenuePeriod($from, $to);
    }

    public function getTopItems($from, $to)
    {
        return $this->userRepository->getTopItemsPeriod($from, $to);
    }

    public function getTopUsers($from, $to)
    {
        return $this->userRepository->getTopUsersPeriod($from, $to);
    }

    public function createBuyOrder($login, $item, $price)
    {
        $userWithInventory = $this->userRepository->getUserByLogin($login);
        $userId = $userWithInventory['user']['id'];
        $balance = $userWithInventory['user']['balance'];
        if ($balance < $price) {
            return "Недостаточно денег на счету.";
        }
        $orders = $this->userRepository->getSaleOrders();
        $order = $this->filterSaleOrder($orders, $price);
        $resultCreateOrder = $this->userRepository->createBuyOrder($userId, $item, $price);
        if ($resultCreateOrder === null) {
            return "Не удалось создать ордер.";
        }
        if (empty($order)) {
            return true;
        }
        if ($this->userRepository->buyItem($order['id'], $userId) === null)
            return "Произошла ошибка.";
        return $this->userRepository->cancelOrder($resultCreateOrder);
    }

    private function filterSaleOrder($orders, $price)
    {
        foreach ($orders as $order) {
            if ($order['price'] <= $price)
                return $order;
        }
        return null;
    }

    public function createSellOrder($inventoryId, $price)
    {
        $inventory = $this->userRepository->getInventory($inventoryId);
        $item = $inventory['item_id'];
        $checkItem = $this->userRepository->checkItem($item);
        if (!empty($checkItem))
            return "Нельзя продать предмет, т.к. 24 часа не прошло.";
        $orders = $this->userRepository->getBuyOrders();
        $order = $this->filterBuyOrder($orders, $price);
        $resultCreateOrder = $this->userRepository->createSaleOrder($inventoryId, $price)['id'];
        if ($resultCreateOrder === null) {
            return "Не удалось создать ордер.";
        }
        if (empty($order)) {
            return true;
        }
        $buyerId = $order['buyer'];
        if ($this->userRepository->buyItem($resultCreateOrder, $buyerId) === null)
            return "Произошла ошибка.";
        return $this->userRepository->cancelOrder($order['id']);
    }

    private function filterBuyOrder($orders, $price)
    {
        foreach ($orders as $order) {
            if ($order['price'] >= $price)
                return $order;
        }
        return null;
    }

    public function buyItem($login, $orderId)
    {
        $userWithInventory = $this->userRepository->getUserByLogin($login);
        $user = $userWithInventory['user'];
        $buyerId = $user['id'];
        $balance = $user['balance'];
        $order = $this->userRepository->getOrderInfo($orderId);
        if (empty($order)) {
            return "Ордер не был найден.";
        }
        $price = $order['price'];
        $sellerId = $order['seller_id'];
        if ($balance < $price) {
            return "Недостаточно средств для покупки.";
        }
        if ($buyerId == $sellerId) {
            return "Нельзя покупать у себя же.";
        }
        return $this->userRepository->buyItem($orderId, $buyerId);
    }

    public function getSaleOrders()
    {
        return $this->userRepository->getSaleOrders();
    }

    public function getBuyOrders()
    {
        return $this->userRepository->getBuyOrders();
    }

    public function cancelOrder($orderId)
    {
        return $this->userRepository->cancelOrder($orderId);
    }

    public function getUserByLogin($login)
    {
        return $this->userRepository->getUserByLogin($login);
    }

    public function getHistory($login, $filter)
    {
        $userWithInventory = $this->userRepository->getUserByLogin($login);
        $userId = $userWithInventory['user']['id'];
        return $this->userRepository->getHistory($userId, $filter);
    }
}