<?php


namespace Presenters;


use Interactors\UserInteractor;

class UserPresenter
{
    /**
     * @var UserInteractor
     */
    private $userInteractor;

    public function __construct($userInteractor)
    {
        $this->userInteractor = $userInteractor;
    }

    public function executeRegistration($login, $password)
    {
        return $this->userInteractor->registration($login, $password);
    }

    public function executeAuthorization($login, $password)
    {
        return $this->userInteractor->authorization($login, $password);
    }

    public function getItems()
    {
        return $this->userInteractor->getItems();
    }

    public function getStatusExchange()
    {
        return $this->userInteractor->getStatusExchange();
    }

    public function getPeriodRevenue($from, $to)
    {
        return $this->userInteractor->getPeriodRevenue($from, $to);
    }

    public function getTopItems($from, $to)
    {
        return $this->userInteractor->getTopItems($from, $to);
    }

    public function getTopUsers($from, $to)
    {
        return $this->userInteractor->getTopUsers($from, $to);
    }

    public function getUserByLogin($login)
    {
        return $this->userInteractor->getUserByLogin($login);
    }

    public function createBuyOrder($login, $item, $price)
    {
        $result = $this->userInteractor->createBuyOrder($login, $item, $price);
        return $this->getResult($result);
    }

    private function getResult($result)
    {
        if (gettype($result) != "string") {
            if ($result === true) {
                return "Операция успешно произведена!";
            }
            return "Что-то произошло не так при создании ордера.";
        }
        return $result;
    }

    public function createSellOrder($inventoryId, $price)
    {
        $result = $this->userInteractor->createSellOrder($inventoryId, $price);
        return $this->getResult($result);
    }

    public function buyItem($login, $orderId)
    {
        $result = $this->userInteractor->buyItem($login, $orderId);
        return $this->getResult($result);
    }

    public function getSales()
    {
        return $this->userInteractor->getSaleOrders();
    }

    public function getPurchases()
    {
        return $this->userInteractor->getBuyOrders();
    }

    public function cancelOrder($orderId)
    {
        return $this->userInteractor->cancelOrder($orderId);
    }

    public function getHistory($login, $filter)
    {
        return $this->userInteractor->getHistory($login, $filter);
    }
}