<?php


namespace Repositories;


use PDO;
use PDOException;
use Repositories\Interfaces\Admin\IOperatingExchange;
use Repositories\Interfaces\Admin\IOperatingUserBalance;
use Repositories\Interfaces\Admin\IOperatingUserItem;

class AdministratorRepository extends UserRepository
    implements IOperatingUserBalance, IOperatingUserItem, IOperatingExchange
{
    public function createItem($name)
    {
        try {
            $query = $this->db->prepare('insert into items (name) values (lower(?))');
            return $query->execute([$name]);
        } catch (PDOException $e) {
            return false;
        }
    }

    public function setItem($userId, $itemId)
    {
        try {
            $query = $this->db->prepare('insert into inventories (user_id, item_id) 
                                    values (?, ?)');
            return $query->execute([$userId, $itemId]);
        } catch (PDOException $e) {
            return false;
        }
    }

    public function changeCommission($commission)
    {
        try {
            $query = $this->db->prepare('update exchanges set commission = ?');
            return $query->execute([$commission]);
        } catch (PDOException $e) {
            return false;
        }
    }

    public function getBalanceExchange()
    {
        $query = $this->db->prepare('select money from exchanges');
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    public function addToBalance($id, $sum)
    {
        try {
            $query = $this->db->prepare('update users set balance = ? + balance
                                        where id = ?');
            return $query->execute([$sum, $id]);
        } catch (PDOException $e) {
            return false;
        }
    }

    public function subtractFromBalance($id, $sum)
    {
        try {
            $query = $this->db->prepare('update users set balance = balance - ? 
                                        where id = ?');
            return $query->execute([$sum, $id]);
        } catch (PDOException $e) {
            return false;
        }
    }
}
