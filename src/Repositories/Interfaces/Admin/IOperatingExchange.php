<?php


namespace Repositories\Interfaces\Admin;


interface IOperatingExchange
{
    public function changeCommission($commission);

    public function getBalanceExchange();
}