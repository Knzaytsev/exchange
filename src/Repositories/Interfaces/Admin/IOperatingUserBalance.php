<?php


namespace Repositories\Interfaces\Admin;


interface IOperatingUserBalance
{
    public function addToBalance($id, $sum);

    public function subtractFromBalance($id, $sum);
}