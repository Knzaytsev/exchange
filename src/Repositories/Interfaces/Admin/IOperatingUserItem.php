<?php


namespace Repositories\Interfaces\Admin;


interface IOperatingUserItem
{
    public function createItem($name);

    public function setItem($userId, $itemId);
}