<?php


namespace Repositories\Interfaces\User;


interface IGettingOrder
{
    public function getSaleOrders();

    public function getBuyOrders();

    public function getOrderInfo($id);
}