<?php


namespace Repositories;


use PDO;
use PDOException;
use Repositories\Interfaces\User\IGettingOrder;
use Repositories\Interfaces\User\IOperatingOrder;

class UserRepository extends AbstractUserRepository
    implements IGettingOrder, IOperatingOrder
{
    public function getBuyOrders()
    {
        $query = $this->db->prepare('select orders.id "id", buyer_id "buyer", name "item", price from orders
                                    join buy_order bo on orders.id = bo.id
                                    join items i on bo.item_id = i.id');
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getSaleOrders()
    {
        $query = $this->db->prepare('select orders.id "id", login "seller", name "item", price from orders 
                                    join sell_order so on orders.id = so.id
                                    join inventories i on so.inventory_id = i.id
                                    join users u on i.user_id = u.id
                                    join items i2 on i.item_id = i2.id');
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function buyItem($orderId, $buyerId)
    {
        try {
            $this->db->beginTransaction();
            /**
             * Получение данных об ордере
             */
            $query = $this->db->prepare('select inventory_id, price, user_id, item_id from sell_order
                                        join orders o on sell_order.id = o.id
                                        join inventories i on sell_order.inventory_id = i.id
                                        where o.id = ?');
            $query->execute([$orderId]);
            $order = $query->fetch(PDO::FETCH_ASSOC);
            $price = $order['price'];
            print_r($orderId);
            $item = $order['item_id'];
            $inventoryId = $order['inventory_id'];
            $sellerId = $order['user_id'];
            /**
             * Получение комиссии
             */
            $exchange = $this->getStatusExchange();
            $commission = $exchange['commission'];
            $priceWithCommission = $price * (1 - $commission);
            $residue = $price * $commission;
            /**
             * Удаление ордера
             */
            $query = $this->db->prepare('delete from orders 
                                        where id in (select id from sell_order where inventory_id = ?)');
            $query->execute([$inventoryId]);
            /**
             * Передача предмета
             */
            $query = $this->db->prepare('update inventories set user_id = ?, count = count + 1 
                                        where id = ?');
            $query->execute([$buyerId, $inventoryId]);
            /**
             * Уменьшение баланса у покупателя
             */
            $query = $this->db->prepare('update users set balance = balance - ? where id = ?');
            $query->execute([$price, $buyerId]);
            /**
             * Увеличение баланса у продавца
             */
            $query = $this->db->prepare('update users set balance = balance + ? where id = ?');
            $query->execute([$priceWithCommission, $sellerId]);
            /**
             * Запись в журнал
             */
            $query = $this->db->prepare('insert into journal (seller_id, buyer_id, item_id, price, date)
                                        values (?, ?, ?, ?, now())');
            $query->execute([$sellerId, $buyerId, $item, $price]);
            /**
             * Увеличение кол-ва денег у биржи
             */
            $query = $this->db->prepare('update exchanges set money = money + ?');
            $query->execute([$residue]);
            return $this->db->commit();
        } catch (PDOException $e) {
            print_r($e->errorInfo);
            $this->db->rollBack();
            return false;
        }
    }

    public function createBuyOrder($buyerId, $itemId, $price)
    {
        try {
            $this->db->beginTransaction();
            $query = $this->db->prepare('insert into orders (price) values (?)');
            $query->execute([$price]);
            $lastId = $this->db->lastInsertId();
            $query = $this->db->prepare('insert into buy_order (id, buyer_id, item_id) values (?, ?, ?)');
            $query->execute([$lastId, $buyerId, $itemId]);
            $query = $this->db->prepare('insert into journal (buyer_id, item_id, price, date) values (?, ?, ?, now())');
            $query->execute([$buyerId, $itemId, $price]);
            $this->db->commit();
            $query = $this->db->query('select max(id) "id" from orders');
            $query->execute();
            return $query->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            $this->db->rollBack();
            return null;
        }
    }

    public function createSaleOrder($inventoryId, $price)
    {
        try {
            $this->db->beginTransaction();
            $query = $this->db->prepare('insert into orders (price) values (?)');
            $query->execute([$price]);
            $lastId = $this->db->lastInsertId();
            $query = $this->db->prepare('insert into sell_order (id, inventory_id) values (?, ?)');
            $query->execute([$lastId, $inventoryId]);
            $query = $this->db->prepare('select user_id, item_id from inventories where id = ?');
            $query->execute([$inventoryId]);
            $inventoryInfo = $query->fetch(PDO::FETCH_ASSOC);
            $seller = $inventoryInfo['user_id'];
            $item = $inventoryInfo['item_id'];
            $query = $this->db->prepare('insert into journal (seller_id, item_id, price, date) values (?, ?, ?, now())');
            $query->execute([$seller, $item, $price]);
            $this->db->commit();
            $query = $this->db->query('select max(id) "id" from orders');
            $query->execute();
            return $query->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            print_r($e->errorInfo);
            $this->db->rollBack();
            return null;
        }
    }

    public function cancelOrder($idOrder)
    {
        try {
            $query = $this->db->prepare('delete from orders where id = ?');
            return $query->execute([$idOrder]);
        } catch (PDOException $e) {
            return false;
        }
    }

    public function updateOrder($orderId, $params)
    {

    }

    public function getHistory($userId, $filter)
    {
        switch ($filter) {
            case "buy":
                $param = 'and seller_id is null';
                break;
            case "sell":
                $param = 'and buyer_id is null';
                break;
            default:
                $param = '';
                break;
        }
        $query = $this->db->prepare('select * from journal where (seller_id = ? or buyer_id = ?) ' . $param . '
                                    or (seller_id is not null and buyer_id is not null)');
        $query->execute([$userId, $userId]);
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getInventory($id)
    {
        $query = $this->db->prepare('select * from inventories where id = ?');
        $query->execute([$id]);
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    public function checkItem($item)
    {
        $query = $this->db->prepare('select * from journal
                                    where hour(timediff(now(), date)) <= 24
                                    and item_id = ?
                                    and seller_id is not null
                                    and buyer_id is not null');
        $query->execute([$item]);
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getOrderInfo($id)
    {
        $query = $this->db->prepare('select * from orders where id = ?');
        $query->execute([$id]);
        return $query->fetch(PDO::FETCH_ASSOC);
    }
}